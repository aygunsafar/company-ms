package az.ingress.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.sql.results.graph.collection.internal.EagerCollectionFetch;
import org.springframework.data.annotation.CreatedDate;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@Table(name="jobs")
public class JobEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    private JobCategory jobCategory;

    @CreationTimestamp
    private LocalDate postedDate;

    private LocalDate expiryDate;

    private Double salary;
    private String location;
    private String contactEmail;
    private String contactPhone;
    private Boolean deleted = false;
    @ManyToOne(fetch = FetchType.EAGER)
    private CompanyEntity company;

}
