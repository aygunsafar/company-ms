package az.ingress.model;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="companies")
public class CompanyEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    UUID id;

    private String name;

    private String phoneNumber;

    private String location;

    private String website;

    private String imageUrl;

    private Boolean deleted = false;

    @Column(columnDefinition = "TEXT")
    private String about;

    @Column(unique = true)
    private String email;

    @ManyToOne
    private CompanyCategory companyCategory;

    @OneToMany(mappedBy = "company", cascade = CascadeType.PERSIST)
    private List<JobEntity> jobs;
}
