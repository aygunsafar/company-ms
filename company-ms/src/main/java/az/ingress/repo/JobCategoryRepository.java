package az.ingress.repo;
import az.ingress.model.JobCategory;
import az.ingress.model.JobEntity;
import org.aspectj.apache.bcel.classfile.Module;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface JobCategoryRepository extends JpaRepository<JobCategory,Long> {

}
