package az.ingress.repo;
import az.ingress.model.CompanyEntity;
import az.ingress.model.JobEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JobRepository extends JpaRepository<JobEntity,Long> {

    Page<JobEntity> findAllByDeletedFalse(Pageable pageable);

}
