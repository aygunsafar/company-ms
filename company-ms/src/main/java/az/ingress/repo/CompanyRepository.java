package az.ingress.repo;
import az.ingress.model.CompanyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CompanyRepository extends JpaRepository<CompanyEntity,UUID> {
    Page<CompanyEntity> findAllByDeletedFalse(Pageable pageable);
}
