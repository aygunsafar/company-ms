package az.ingress.repo;

import az.ingress.model.CompanyCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyCategoryRepository extends JpaRepository<CompanyCategory,Long> {

}
