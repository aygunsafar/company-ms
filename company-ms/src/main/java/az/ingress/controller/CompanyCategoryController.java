package az.ingress.controller;

import az.ingress.dto.companyCategoryDto.CompanyCategoryRequestDto;
import az.ingress.dto.companyCategoryDto.CompanyCategoryResponseDto;
import az.ingress.service.categoryService.CategoryService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/company-categories")
@RequiredArgsConstructor
@Slf4j
public class CompanyCategoryController {
    private final CategoryService<CompanyCategoryRequestDto,CompanyCategoryResponseDto> companyCategoryService;

    @GetMapping("/{id}")
    public ResponseEntity<CompanyCategoryResponseDto> getCategoryById(@PathVariable Long id) {
        log.trace("Get Company Category by id {}", id);
        return ResponseEntity.ok(companyCategoryService.getCategoryById(id));
    }

    @PostMapping
    public ResponseEntity<CompanyCategoryResponseDto> createCategory(@RequestBody @Valid CompanyCategoryRequestDto companyCategoryRequestDto) {
        log.trace("Create Company Category {}", companyCategoryRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(companyCategoryService.createCategory(companyCategoryRequestDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<CompanyCategoryResponseDto> updateCategory(@PathVariable Long id, @RequestBody @Valid CompanyCategoryRequestDto companyCategoryRequestDto) {
        log.trace("Update Company Category by id {} {}", id, companyCategoryRequestDto);
        return ResponseEntity.ok().body(companyCategoryService.updateCategory(id, companyCategoryRequestDto));
    }

    @GetMapping()
    public ResponseEntity<Page<CompanyCategoryResponseDto>> getCategoryList(Pageable pageable) {
        return ResponseEntity.ok().body(companyCategoryService.getCategoryList(pageable));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) {
        log.trace("Delete Company Category by id {}", id);
        companyCategoryService.deleteCategory(id);
        return ResponseEntity.noContent().build();
    }
}
