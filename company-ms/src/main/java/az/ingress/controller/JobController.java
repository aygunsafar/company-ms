package az.ingress.controller;

import az.ingress.dto.jobDto.JobRequestDto;
import az.ingress.dto.jobDto.JobResponseDto;
import az.ingress.service.jobService.JobService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/jobs")
@RequiredArgsConstructor
public class JobController {
    private final JobService jobService;

    @PostMapping
    public JobResponseDto createJob(@Valid @RequestBody JobRequestDto jobRequestDto) {
        return jobService.createJob(jobRequestDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<JobResponseDto> getJob(@PathVariable Long id) {
        return ResponseEntity.ok().body(jobService.getJob(id));
    }

    @GetMapping
    public ResponseEntity<Page<JobResponseDto>> getJobList(Pageable pageable) {
        return ResponseEntity.ok().body(jobService.getJobList(pageable));
    }

    @PutMapping("/{id}")
    public ResponseEntity<JobResponseDto> updateJob(@PathVariable Long id, @RequestBody JobRequestDto jobRequestDto) {
        return ResponseEntity.ok().body(jobService.updateJob(id, jobRequestDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteJob(@PathVariable Long id) {
        jobService.deleteJob(id);
        return ResponseEntity.noContent().build();
    }
}
