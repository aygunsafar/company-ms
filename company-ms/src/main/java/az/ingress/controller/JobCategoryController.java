package az.ingress.controller;

import az.ingress.dto.jobCategoryDto.JobCategoryRequestDto;
import az.ingress.dto.jobCategoryDto.JobCategoryResponseDto;
import az.ingress.service.categoryService.CategoryService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/job-categories")
@RequiredArgsConstructor
@Slf4j
public class JobCategoryController {
    private final CategoryService<JobCategoryRequestDto,JobCategoryResponseDto> jobCategoryService;

    @GetMapping("/{id}")
    public ResponseEntity<JobCategoryResponseDto> getCategoryById(@PathVariable Long id) {
        log.trace("Get Job Category by id {}", id);
        return ResponseEntity.ok(jobCategoryService.getCategoryById(id));
    }

    @PostMapping
    public ResponseEntity<JobCategoryResponseDto> createCategory(@RequestBody @Valid JobCategoryRequestDto jobCategoryRequestDto) {
        log.trace("Create Job Category {}", jobCategoryRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(jobCategoryService.createCategory(jobCategoryRequestDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<JobCategoryResponseDto> updateCategory(@PathVariable Long id, @RequestBody @Valid JobCategoryRequestDto jobCategoryRequestDto) {
        log.trace("Update Job Category by id {} {}", id, jobCategoryRequestDto);
        return ResponseEntity.ok().body(jobCategoryService.updateCategory(id, jobCategoryRequestDto));
    }

    @GetMapping()
    public ResponseEntity<Page<JobCategoryResponseDto>> getCategoryList(Pageable pageable) {
        return ResponseEntity.ok().body(jobCategoryService.getCategoryList(pageable));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) {
        log.trace("Delete Job Category by id {}", id);
        jobCategoryService.deleteCategory(id);
        return ResponseEntity.noContent().build();
    }
}
