package az.ingress.controller;

import az.ingress.dto.companyDto.CompanyRequestDto;
import az.ingress.dto.companyDto.CompanyResponseDto;
import az.ingress.service.companyService.CompanyService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequestMapping("/companies")
@RequiredArgsConstructor
public class CompanyController {
    private final CompanyService companyService;

    @PutMapping("/{id}")
    public ResponseEntity<CompanyResponseDto> createCompany(
            @PathVariable UUID id,
            @RequestPart("file") MultipartFile file,
           @Valid @RequestPart("json") CompanyRequestDto companyRequestDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(companyService.createOrUpdateCompany(id, file, companyRequestDto));
    }

    @GetMapping("/{companyId}")
    public ResponseEntity<CompanyResponseDto> getCompany(@PathVariable UUID companyId) {
        return ResponseEntity.ok(companyService.getCompanyById(companyId));
    }

    @GetMapping()
    public ResponseEntity<Page<CompanyResponseDto>> getCompanyList(Pageable pageable) {
        return ResponseEntity.ok().body(companyService.getCompanyList(pageable));
    }

    @DeleteMapping("/{companyId}")
    public ResponseEntity<Void> deleteCompany(@PathVariable UUID companyId) {
        companyService.deleteCompany(companyId);
        return ResponseEntity.noContent().build();
    }

}
