package az.ingress.exception;

import java.util.Map;

public class ApplicationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final ErrorResponse errorResponse;
    private final Map<String, Object> messageArguments;

    public ApplicationException(ErrorResponse errorResponse) {
        super(errorResponse.getMessage());
        this.errorResponse = errorResponse;
        this.messageArguments = null;
    }

    public ApplicationException(ErrorResponse errorResponse, Map<String, Object> messageArguments) {
        super(errorResponse.getMessage());
        this.errorResponse = errorResponse;
        this.messageArguments = messageArguments;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    public Map<String, Object> getMessageArguments() {
        return messageArguments;
    }
}
