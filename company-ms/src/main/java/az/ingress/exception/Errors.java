package az.ingress.exception;


import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.BAD_GATEWAY;

public enum Errors implements ErrorResponse {
    FILE_UPLOAD_EXCEPTION("FILE_UPLOAD_EXCEPTION", BAD_GATEWAY, "File upload error"),
    COMPANY_NOT_FOUND("COMPANY_NOT_FOUND_EXCEPTION", HttpStatus.NOT_FOUND, "Company with id [id] not found"),
    COMPANY_CATEGORY_NOT_FOUND("COMPANY_CATEGORY_NOT_FOUND_EXCEPTION", HttpStatus.NOT_FOUND, "Company Category with id [id] not found"),
    JOB_NOT_FOUND("JOB_NOT_FOUND_EXCEPTION", HttpStatus.NOT_FOUND, "Job with id [id] not found"),
    JOB_CATEGORY_NOT_FOUND("JOB_CATEGORY_NOT_FOUND_EXCEPTION", HttpStatus.NOT_FOUND, "Job Category with id [id] not found"),
    IMAGE_REQUIRED("IMAGE_REQUIRED", HttpStatus.BAD_REQUEST, "Image file is required");
    String key;
    HttpStatus httpStatus;
    String message;

    Errors(String key, HttpStatus httpStatus, String message) {
        this.message = message;
        this.key = key;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }


}
