package az.ingress.client;

import az.ingress.exception.ApplicationException;
import az.ingress.exception.Errors;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA;

@Slf4j
@Component
@FieldDefaults(makeFinal = true, level = PRIVATE)
public class FileClient {
    String baseUrl;
    WebClient webClient;

    public FileClient(@Value("${client.urls.ms-file}") String baseUrl, WebClient.Builder webClientBuilder) {
        this.baseUrl = baseUrl;
        this.webClient = webClientBuilder.baseUrl(this.baseUrl).build();
    }

    public String uploadFile(UUID id, MultipartFile file) {
        String url;
        try {
            var bodyBuilder = prepareMultipartBodyBuilder(file);
            var block = webClient.put().uri(uriBuilder -> uriBuilder.path("/files/file/{id}").build(id)).contentType(MULTIPART_FORM_DATA).header(CONTENT_TYPE, MULTIPART_FORM_DATA.toString()).accept(APPLICATION_JSON).body(BodyInserters.fromMultipartData(bodyBuilder.build())).retrieve().bodyToMono(Map.class).block();
            assert block != null;
            url = block.get("url").toString();
        } catch (Exception e) {
            throw new ApplicationException(Errors.FILE_UPLOAD_EXCEPTION);
        }
        return url;
    }

    private MultipartBodyBuilder prepareMultipartBodyBuilder(MultipartFile file) {
        var bodyBuilder = new MultipartBodyBuilder();
        try {
            bodyBuilder.part("file", new ByteArrayResource(file.getBytes())).filename(Objects.requireNonNull(file.getOriginalFilename()));
        } catch (IOException e) {
            log.error("IOException => ", e);
        }
        return bodyBuilder;
    }
}
