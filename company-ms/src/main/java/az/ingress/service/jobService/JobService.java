package az.ingress.service.jobService;

import az.ingress.dto.jobDto.JobRequestDto;
import az.ingress.dto.jobDto.JobResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

public interface JobService {
    JobResponseDto createJob(JobRequestDto jobRequestDto);
    JobResponseDto getJob(Long id);
    JobResponseDto updateJob(Long id, JobRequestDto jobRequestDto);

    Page<JobResponseDto> getJobList(Pageable pageable);
    void deleteJob(Long id);

}
