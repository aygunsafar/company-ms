package az.ingress.service.jobService;

import az.ingress.dto.jobDto.JobRequestDto;
import az.ingress.dto.jobDto.JobResponseDto;
import az.ingress.exception.ApplicationException;
import az.ingress.exception.Errors;
import az.ingress.model.CompanyEntity;
import az.ingress.model.JobCategory;
import az.ingress.model.JobEntity;
import az.ingress.repo.CompanyRepository;
import az.ingress.repo.JobCategoryRepository;
import az.ingress.repo.JobRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class JobServiceImpl implements JobService {
    private final JobRepository jobRepository;
    private final CompanyRepository companyRepository;
    private final JobCategoryRepository jobCategoryRepository;
    private final ModelMapper modelMapper;

    @Override
    public JobResponseDto createJob(JobRequestDto jobRequestDto) {
        JobEntity jobEntity=modelMapper.map(jobRequestDto,JobEntity.class);
        jobEntity.setJobCategory(findJobCategoryById(jobRequestDto.getJobCategory_id()));
        jobEntity.setCompany(findCompanyById(jobRequestDto.getCompany_id()));
        return modelMapper.map(jobRepository.save(jobEntity),JobResponseDto.class);
    }

    @Override
    public JobResponseDto getJob(Long id) {
        return modelMapper.map(findJobById(id),JobResponseDto.class);
    }

    @Override
    public JobResponseDto updateJob(Long id, JobRequestDto jobRequestDto) {
        JobEntity jobEntity= findJobById(id);
        if(jobRequestDto!=null){
            updateCompanyUtil(jobEntity,jobRequestDto);
        }
        return modelMapper.map(jobEntity,JobResponseDto.class);
    }

    @Override
    public Page<JobResponseDto> getJobList(Pageable pageable) {
        return jobRepository.findAllByDeletedFalse(pageable).map(jobEntity -> modelMapper.map(jobEntity,JobResponseDto.class));
    }

    @Override
    public void deleteJob(Long id) {
        final JobEntity jobById = findJobById(id);
        jobById.setDeleted(true);
        jobRepository.save(jobById);
    }

    private void updateCompanyUtil(JobEntity jobEntity, JobRequestDto jobRequestDto) {
        if(jobRequestDto.getCompany_id()!=null){
            jobEntity.setCompany(findCompanyById(jobRequestDto.getCompany_id()));
        }
        if(jobRequestDto.getJobCategory_id()!=null){
            jobEntity.setJobCategory(findJobCategoryById(jobRequestDto.getJobCategory_id()));
        }
        if(jobRequestDto.getSalary()!=null){
            jobEntity.setSalary(jobRequestDto.getSalary());
        }
        if(jobRequestDto.getDescription()!=null){
            jobEntity.setDescription(jobRequestDto.getDescription());
        }
        if(jobRequestDto.getLocation()!=null){
            jobEntity.setLocation(jobRequestDto.getLocation());
        }
        if(jobRequestDto.getTitle()!=null){
            jobEntity.setTitle(jobRequestDto.getTitle());
        }
        if(jobRequestDto.getContactPhone()!=null){
            jobEntity.setContactPhone(jobRequestDto.getContactPhone());
        }
        if(jobRequestDto.getContactEmail()!=null){
            jobEntity.setContactEmail(jobRequestDto.getContactEmail());
        }
        if(jobRequestDto.getExpiryDate()!=null){
            jobEntity.setExpiryDate(jobRequestDto.getExpiryDate());
        }

    }


    private JobEntity findJobById(Long id){
        return jobRepository.findById(id).orElseThrow(()-> new ApplicationException(Errors.JOB_NOT_FOUND));
    }
    private CompanyEntity findCompanyById(UUID id){
        return companyRepository.findById(id).orElseThrow(()-> new ApplicationException(Errors.COMPANY_NOT_FOUND));
    }
    private JobCategory findJobCategoryById(Long id){
        return jobCategoryRepository.findById(id).orElseThrow(()-> new ApplicationException(Errors.JOB_CATEGORY_NOT_FOUND));
    }

}
