package az.ingress.service.categoryService;


import az.ingress.dto.companyCategoryDto.CompanyCategoryRequestDto;
import az.ingress.dto.companyCategoryDto.CompanyCategoryResponseDto;
import az.ingress.exception.ApplicationException;
import az.ingress.exception.Errors;
import az.ingress.model.CompanyCategory;
import az.ingress.repo.CompanyCategoryRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CompanyCategoryServiceImpl implements CategoryService<CompanyCategoryRequestDto,CompanyCategoryResponseDto> {

    private final CompanyCategoryRepository companyCategoryRepository;
    private final ModelMapper modelMapper;

    @Override
    public CompanyCategoryResponseDto createCategory(CompanyCategoryRequestDto companyCategoryRequestDto) {
        final CompanyCategory category = modelMapper.map(companyCategoryRequestDto, CompanyCategory.class);
        return modelMapper.map(companyCategoryRepository.save(category), CompanyCategoryResponseDto.class);
    }

    @Override
    public CompanyCategoryResponseDto getCategoryById(Long id) {
        return modelMapper.map(findCompanyCategoryById(id), CompanyCategoryResponseDto.class);
    }

    @Override
    public void deleteCategory(Long id) {
        companyCategoryRepository.delete(findCompanyCategoryById(id));
    }

    @Override
    public CompanyCategoryResponseDto updateCategory(Long id, CompanyCategoryRequestDto companyCategoryRequestDto) {
        final CompanyCategory companyCategoryById = findCompanyCategoryById(id);
        companyCategoryById.setName(companyCategoryRequestDto.getName());
        return modelMapper.map(companyCategoryRepository.save(companyCategoryById),CompanyCategoryResponseDto.class);
    }

    @Override
    public Page<CompanyCategoryResponseDto> getCategoryList(Pageable pageable) {
        return companyCategoryRepository.findAll(pageable).map(category->modelMapper.map(category,CompanyCategoryResponseDto.class));
    }

    private CompanyCategory findCompanyCategoryById(Long id) {
        return companyCategoryRepository.findById(id).orElseThrow(() -> new ApplicationException(Errors.COMPANY_CATEGORY_NOT_FOUND));
    }
}
