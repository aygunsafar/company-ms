package az.ingress.service.categoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoryService<RequestDto, ResponseDto> {
    ResponseDto createCategory(RequestDto requestDto);
    ResponseDto getCategoryById(Long id);
    void deleteCategory(Long id);
    ResponseDto updateCategory(Long id, RequestDto requestDto);
    Page<ResponseDto> getCategoryList(Pageable pageable);
}
