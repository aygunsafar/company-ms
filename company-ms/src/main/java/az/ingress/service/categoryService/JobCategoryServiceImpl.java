package az.ingress.service.categoryService;

import az.ingress.dto.jobCategoryDto.JobCategoryRequestDto;
import az.ingress.dto.jobCategoryDto.JobCategoryResponseDto;
import az.ingress.exception.ApplicationException;
import az.ingress.exception.Errors;
import az.ingress.model.JobCategory;
import az.ingress.repo.JobCategoryRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobCategoryServiceImpl implements CategoryService<JobCategoryRequestDto,JobCategoryResponseDto> {

    private final JobCategoryRepository jobCategoryRepository;
    private final ModelMapper modelMapper;
    @Override
    public JobCategoryResponseDto createCategory(JobCategoryRequestDto jobCategoryRequestDto) {
        final JobCategory category = modelMapper.map(jobCategoryRequestDto, JobCategory.class);
        return modelMapper.map(jobCategoryRepository.save(category), JobCategoryResponseDto.class);
    }

    @Override
    public JobCategoryResponseDto getCategoryById(Long id) {
        return modelMapper.map(findJobCategoryById(id),JobCategoryResponseDto.class);
    }

    @Override
    public void deleteCategory(Long id) {
        jobCategoryRepository.delete(findJobCategoryById(id));
    }

    @Override
    public JobCategoryResponseDto updateCategory(Long id, JobCategoryRequestDto jobCategoryRequestDto) {
        final JobCategory jobCategory = findJobCategoryById(id);
        jobCategory.setName(jobCategoryRequestDto.getName());
        return modelMapper.map(jobCategoryRepository.save(jobCategory),JobCategoryResponseDto.class);
    }

    @Override
    public Page<JobCategoryResponseDto> getCategoryList(Pageable pageable) {
        return jobCategoryRepository.findAll(pageable).map(category->modelMapper.map(category, JobCategoryResponseDto.class));
    }

    private JobCategory findJobCategoryById(Long id){
        return jobCategoryRepository.findById(id).orElseThrow(()->new ApplicationException(Errors.JOB_CATEGORY_NOT_FOUND));
    }
}
