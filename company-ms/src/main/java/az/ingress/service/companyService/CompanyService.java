package az.ingress.service.companyService;

import az.ingress.dto.companyDto.CompanyRequestDto;
import az.ingress.dto.companyDto.CompanyResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface CompanyService {
    CompanyResponseDto createOrUpdateCompany(UUID id, MultipartFile file, CompanyRequestDto companyRequestDto);

    CompanyResponseDto getCompanyById(UUID id);

    Page<CompanyResponseDto> getCompanyList(Pageable pageable);

    void deleteCompany(UUID id);

}
