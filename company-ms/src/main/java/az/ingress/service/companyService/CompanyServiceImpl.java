package az.ingress.service.companyService;

import az.ingress.client.FileClient;
import az.ingress.dto.companyDto.CompanyRequestDto;
import az.ingress.dto.companyDto.CompanyResponseDto;
import az.ingress.exception.ApplicationException;
import az.ingress.exception.Errors;
import az.ingress.model.CompanyCategory;
import az.ingress.model.CompanyEntity;
import az.ingress.repo.CompanyCategoryRepository;
import az.ingress.repo.CompanyRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {
    private final CompanyRepository companyRepository;
    private final ModelMapper modelMapper;
    private final CompanyCategoryRepository companyCategoryRepository;
    private final FileClient fileClient;

    @Override
    public CompanyResponseDto createOrUpdateCompany(UUID id, MultipartFile file, CompanyRequestDto request) {

        CompanyEntity company = companyRepository.findById(id).map(c -> {
                    modelMapper.map(request, c);
                    return c;
                }
        ).orElseGet(() -> {
            CompanyEntity newCompany = modelMapper.map(request, CompanyEntity.class);
            newCompany.setId(id);
            newCompany.setCompanyCategory(findCompanyCategoryById(request.getCompanyCategory_id()));
            return newCompany;
        });
        if (file != null) {
            String url = fileClient.uploadFile(id, file);
            company.setImageUrl(url);
        }
        return modelMapper.map(companyRepository.save(company), CompanyResponseDto.class);
    }

    @Override
    public CompanyResponseDto getCompanyById(UUID id) {
        return modelMapper.map(findCompanyById(id), CompanyResponseDto.class);
    }

    @Override
    public Page<CompanyResponseDto> getCompanyList(Pageable pageable) {
        Page<CompanyEntity> companiesPage = companyRepository.findAllByDeletedFalse(pageable);
        return companiesPage.map(company -> modelMapper.map(company, CompanyResponseDto.class));
    }

    @Override
    public void deleteCompany(UUID id) {
        final CompanyEntity company = findCompanyById(id);
        company.setDeleted(true);
        companyRepository.save(company);
    }

    private CompanyEntity findCompanyById(UUID id) {
        return companyRepository.findById(id).orElseThrow(() -> new ApplicationException(Errors.COMPANY_NOT_FOUND));
    }

    private CompanyCategory findCompanyCategoryById(Long id) {
        return companyCategoryRepository.findById(id).orElseThrow(() -> new ApplicationException(Errors.COMPANY_CATEGORY_NOT_FOUND));
    }

}
