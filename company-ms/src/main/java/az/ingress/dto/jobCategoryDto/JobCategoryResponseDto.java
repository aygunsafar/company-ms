package az.ingress.dto.jobCategoryDto;

import lombok.Data;

@Data
public class JobCategoryResponseDto {
    private Long id;
    private String name;
}
