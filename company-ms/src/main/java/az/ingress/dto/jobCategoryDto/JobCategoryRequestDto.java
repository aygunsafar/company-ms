package az.ingress.dto.jobCategoryDto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class JobCategoryRequestDto {
    @NotBlank
    private String name;
}
