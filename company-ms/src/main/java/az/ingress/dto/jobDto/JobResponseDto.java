package az.ingress.dto.jobDto;

import az.ingress.dto.companyDto.CompanyResponseDto;
import az.ingress.dto.jobCategoryDto.JobCategoryResponseDto;
import az.ingress.model.CompanyEntity;
import lombok.Data;
import java.time.LocalDate;
import java.util.Set;

@Data
public class JobResponseDto {

    private Long id;
    private String title;
    private String description;
    private JobCategoryResponseDto jobCategory;
    private LocalDate expiryDate;
    private Double salary;
    private String location;
    private String contactEmail;
    private String contactPhone;
    private CompanyResponseDto company;
}
