package az.ingress.dto.jobDto;
import com.google.firebase.database.annotations.NotNull;
import jakarta.validation.constraints.*;
import lombok.Data;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Data
public class JobRequestDto {
    private UUID company_id;

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @Future
    private LocalDate expiryDate;

    @NotNull
    @DecimalMin("0.0")
    private Double salary;

    @NotBlank
    private String location;

    @Email
    @NotBlank
    private String contactEmail;

    @Pattern(regexp = "^\\+994[0-9]{9}$",message = "Please enter a valid phone number starting with +994 followed by 9 digits.")
    @NotBlank
    private String contactPhone;

    @NotNull
    private Long jobCategory_id;
}
