package az.ingress.dto.companyCategoryDto;
import lombok.Data;

@Data
public class CompanyCategoryResponseDto {
    private Long id;
    private String name;
}
