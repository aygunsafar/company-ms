package az.ingress.dto.companyCategoryDto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CompanyCategoryRequestDto {
    @NotBlank
    private String name;
}
