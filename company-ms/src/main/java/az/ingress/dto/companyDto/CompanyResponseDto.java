package az.ingress.dto.companyDto;

import az.ingress.dto.companyCategoryDto.CompanyCategoryResponseDto;
import lombok.Data;

import java.util.UUID;

@Data
public class CompanyResponseDto {

    private UUID id;
    private String name;
    private String phoneNumber;
    private String location;
    private String website;
    private String imageUrl;
    private String about;
    private String email;
    private CompanyCategoryResponseDto companyCategory;

}
