package az.ingress.dto.companyDto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;


@Data
public class CompanyRequestDto {
    private Long companyCategory_id;

    @NotBlank
    private String name;

    @Pattern(regexp = "\\+994\\d{9}",message = "Please enter a valid phone number starting with +994 followed by 9 digits.")
    private String phoneNumber;

    @NotBlank
    private String location;

    private String website;

    @NotBlank
    private String about;

    @NotBlank
    @Email
    private String email;
}
