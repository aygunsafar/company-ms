package az.ingress.filesms.exception;

public class FileUpdateException extends RuntimeException {
    public FileUpdateException(String message) {
        super(message);
    }
}
