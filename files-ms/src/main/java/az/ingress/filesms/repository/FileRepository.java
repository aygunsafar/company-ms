package az.ingress.filesms.repository;

import az.ingress.filesms.entity.FileRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FileRepository extends JpaRepository<FileRequest, UUID> {
}
