package az.ingress.filesms.controller;

import az.ingress.filesms.dto.FileResponseDto;
import az.ingress.filesms.service.FileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;

import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@Slf4j
@RequestMapping("/file")
@RequiredArgsConstructor
public class FileController {
    private final FileService fileService;

    @GetMapping("/{id}")
    public Mono<ResponseEntity<FileResponseDto>> getFileById(@PathVariable UUID id) {
        log.trace("Getting file by ID: {}", id);
        return fileService.getFileById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping(value = "/{id}", consumes = MULTIPART_FORM_DATA_VALUE)
    public Mono<Map<String, Object>> upload(@PathVariable UUID id,
                                            @RequestPart("file") MultipartFile file) {
        log.trace("HIT -/upload | File Name : {}", file.getOriginalFilename());
        return fileService.uploadOrUpdateFile(id, file)
                .map(url -> Map.of("url", url));
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteFile(@PathVariable UUID id) {
        log.trace("Deleting file with ID: {}", id);
        return fileService.deleteFile(id);
    }
}



