package az.ingress.filesms.service.impl;

import az.ingress.filesms.dto.FileResponseDto;
import az.ingress.filesms.entity.FileRequest;
import az.ingress.filesms.repository.FileRepository;
import az.ingress.filesms.service.FileService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.StorageOptions;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Service
@FieldDefaults(makeFinal = true, level = PRIVATE)
public class FileServiceImpl implements FileService {
    FileRepository fileRepository;
    ModelMapper modelMapper;
    String bucketName;
    String imageUrl;

    public FileServiceImpl(
            @Value("${firebase.bucket-name}")
            String bucketName,
            @Value("${firebase.image-url}")
            String imageUrl,
            FileRepository fileRepository,
            ModelMapper modelMapper) {
        this.bucketName = bucketName;
        this.imageUrl = imageUrl;
        this.fileRepository = fileRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Mono<String> uploadOrUpdateFile(UUID id, MultipartFile file) {
        return Mono.just(file).publishOn(Schedulers.boundedElastic()).publishOn(Schedulers.boundedElastic())
                .publishOn(Schedulers.boundedElastic()).flatMap(f -> {
                    var fileExtension = Objects.requireNonNull(StringUtils.getFilenameExtension(f.getOriginalFilename()));
                    var fileName = generateFileName(id, fileExtension);
                    var serviceAccount = new ClassPathResource("fir-project.json");
                    var blobId = BlobId.of(bucketName, fileName);
                    var blobInfo = BlobInfo.newBuilder(blobId).setContentType(f.getContentType()).build();
                    try {
                        var credentials = GoogleCredentials.fromStream(serviceAccount.getInputStream());
                        var storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
                        storage.create(blobInfo, f.getBytes());
                        var fileRequest = fileRepository.findById(id)
                                .map(existingFile -> {
                                    existingFile.setFileName(fileName);
                                    existingFile.setImageUrl(String.format(imageUrl, fileName));
                                    return existingFile;
                                })
                                .orElseGet(() -> FileRequest.builder()
                                        .uuid(id)
                                        .fileName(fileName)
                                        .imageUrl(String.format(imageUrl, fileName))
                                        .build()
                                );
                        var savedFileRequest = fileRepository.save(fileRequest);
                        log.trace("File uploaded successfully: {}", savedFileRequest.getImageUrl());
                        return Mono.just(savedFileRequest.getImageUrl());
                    } catch (Exception e) {
                        log.error("Error uploading file", e);
                        return Mono.error(e);
                    }
                });
    }

    @Override
    public Mono<FileResponseDto> getFileById(UUID id) {
        Optional<FileRequest> fileRequest = fileRepository.findById(id);

        if (fileRequest.isPresent()) {
            FileRequest file = fileRequest.get();
            FileResponseDto dto = modelMapper.map(file, FileResponseDto.class);
            log.trace("File with ID {} retrieved successfully", id);
            return Mono.just(dto);
        } else {
            log.trace("File with ID {} not found", id);
            return Mono.empty();
        }
    }

    @Override
    public Mono<Void> deleteFile(UUID id) {
        log.trace("Deleting file with ID: {}", id);
        return Mono.fromRunnable(() -> fileRepository.deleteById(id));
    }

    private String generateFileName(UUID id, String fileExtension) {
        return id + "." + fileExtension;
    }
}
