package az.ingress.filesms.service;

import az.ingress.filesms.dto.FileResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public interface FileService {

    Mono<FileResponseDto> getFileById(UUID id);


    Mono<Void> deleteFile(UUID id);

    Mono<String> uploadOrUpdateFile(UUID id, MultipartFile file);
}